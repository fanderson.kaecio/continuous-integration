Dado('que eu acesse a página principal') do
 visit 'https://www.dio.me/sign-in'
end

Quando('eu faço login com {string} e {string}') do |email, password|
  find('input[placeholder=E-mail]').set email
  find('input[placeholder=Senha]').set password
  click_button 'Entrar'
end

Então('devo ser autenticado com sucesso e ver a mensagem na página {string}') do |mensagem|
  puts mensagem
  #expect(page).to have_css("h5", text: 'FANDERSON VALENCIO')
  #expect(page).to have_content(@mensagem),
  expect(page).to have_content mensagem
end