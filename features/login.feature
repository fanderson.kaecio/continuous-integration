#language:pt

Funcionalidade: Login
Para que eu possa logar com sucesso na página https://www.dio.me/sign-in

Cenário: Usuário deve se autorizado
Dado que eu acesse a página principal
Quando eu faço login com "fanderson.kaecio@gmail.com" e "Canada2017"
Então devo ser autenticado com sucesso e ver a mensagem na página "É HORA DE ACELERAR O SEU APRENDIZADO"

